import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SignosVitales } from '../../_model/signos-vitales';
import { SignosVitalesService } from '../../_service/signosvitales.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-signos-vitales',
  templateUrl: './signos-vitales.component.html',
  styleUrls: ['./signos-vitales.component.css']
})
export class SignosVitalesComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource: MatTableDataSource<SignosVitales>;
  displayedColumns: string[] = ['idSignosVitales', 'paciente', 'fecha', 'acciones'];
  cantidad: number = 0;

  constructor(
    private signosService: SignosVitalesService,
    private snackBar: MatSnackBar,
    public route: ActivatedRoute
  ) { }

  ngOnInit(): void {

    this.signosService.listar().subscribe(data => {
      this.crearTabla(data);
    });

    /*this.signosService.listarPageable(0, 10).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort = this.sort;
    });*/

    this.signosService.getSignosCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.signosService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000,
        verticalPosition: "top",
        horizontalPosition: "right"
      });
    });
  }

  eliminar(id: number) {
    this.signosService.eliminar(id).subscribe(() => {
      this.signosService.listar().subscribe(data => {
        this.signosService.setSignosCambio(data);
        this.signosService.setMensajeCambio('El registro se eliminó...');
      });
    });
  }

  crearTabla(data: SignosVitales[]) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  filtrar(valor: string) {
    this.dataSource.filter = valor.trim().toLowerCase();
  }

}
