import { Component, OnInit } from '@angular/core';
import { SignosVitales } from '../../../_model/signos-vitales';
import { Paciente } from '../../../_model/paciente';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { PacienteService } from '../../../_service/paciente.service';
import { SignosVitalesService } from '../../../_service/signosvitales.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

@Component({
  selector: 'app-signos-edicion',
  templateUrl: './signos-edicion.component.html',
  styleUrls: ['./signos-edicion.component.css']
})
export class SignosEdicionComponent implements OnInit {

  form: FormGroup;
  public signos:SignosVitales=<SignosVitales>{} ;
  public idSignos:number;
  public edicion:boolean;

  pacientes: Paciente[];
  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();

  public pacienteSeleccionado: Paciente;

  myControlPaciente: FormControl = new FormControl();

  pacientesFiltrados$: Observable<Paciente[]>;


  constructor(
    private pacienteService: PacienteService,
    private signosService: SignosVitalesService,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      'paciente': this.myControlPaciente,
      'fecha': new FormControl(new Date()),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmo': new FormControl('')
    });
    this.listarPacientes();
    this.route.params.subscribe((data: Params) => {
      this.idSignos = data['id'];
      this.edicion = data['id'] != null;
      if (this.edicion){
        this.signosService.listarPorId(this.idSignos)
                  .subscribe(async data=>{  this.signos= await data
                   //console.log(await this.empresa);
                   this.pacienteSeleccionado = await this.signos.paciente;
                   this.myControlPaciente.setValue(this.pacienteSeleccionado);
                   this.fechaSeleccionada = moment(this.signos.fecha).toDate(); 
                  });
      }
    });
    this.pacientesFiltrados$ = this.myControlPaciente.valueChanges.pipe(map(val => this.filtrarPacientes(val)));
  }

  listarPacientes() {
    this.pacienteService.listar().subscribe(data => {
      this.pacientes = data;
    });
  }
  mostrarPaciente(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  filtrarPacientes(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(el =>
        el.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || el.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || el.dni.includes(val.dni)
      );
    }
    return this.pacientes.filter(el =>
      el.nombres.toLowerCase().includes(val?.toLowerCase()) || el.apellidos.toLowerCase().includes(val?.toLowerCase()) || el.dni.includes(val)
    );
  }

  grabar() {

    this.pacienteSeleccionado = this.form.value['paciente'];
    this.signos.paciente=this.pacienteSeleccionado;//this.pacienteSeleccionado;
    this.signos.fecha= moment(this.fechaSeleccionada).format('YYYY-MM-DDTHH:mm:ss');
    console.log(this.signos.paciente);
    console.log(this.signos.fecha);
    //recuperar la fecha a string 
    if (this.edicion) {
      //MODIFICAR
      this.signosService.modificar(this.signos).subscribe( ()=> {
        this.signosService.listar().subscribe(data => {
          this.signosService.setSignosCambio(data);
          this.signosService.setMensajeCambio('Los datos se modificaron correctamente.');
        })
      })
    } else {
      //REGISTRAR
      this.signosService.registrar(this.signos).subscribe(() => {
        this.signosService.listar().subscribe(data => {
          this.signosService.setSignosCambio(data);
          this.signosService.setMensajeCambio('Los datos se registraron correctamente.');
        })
      });
  
     }
    this.router.navigate(['/pages/signosvitales']); 
  }

  cancelar() {
    this.limpiarControles();
    this.router.navigate(['/pages/signosvitales']); 
  }

  limpiarControles() {

    this.pacienteSeleccionado = null;
    this.fechaSeleccionada = new Date();
 
    //para autocompletes
    this.myControlPaciente.reset();

  }

}
