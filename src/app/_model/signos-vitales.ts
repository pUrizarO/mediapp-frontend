import { Paciente } from './paciente';
export class SignosVitales {
    idSignosVitales: number;
    fecha: string;
    pulso: string;
    ritmoRespiratorio: string;
    temperatura: string;
    paciente: Paciente
}